package com.testtask.conferencer.repository;

import java.util.List;

import com.testtask.conferencer.entity.Participant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ParticipantRepository
 */
@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    public List<Participant> findByConferenceId(Long conferenceId);

    public long countByConferenceId(Long conferenceId);

 }