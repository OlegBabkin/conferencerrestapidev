package com.testtask.conferencer.repository;

import com.testtask.conferencer.entity.Conference;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long> { }