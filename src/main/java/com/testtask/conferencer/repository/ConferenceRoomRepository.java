package com.testtask.conferencer.repository;

import java.util.List;

import com.testtask.conferencer.entity.ConferenceRoom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConferenceRoomRepository extends JpaRepository<ConferenceRoom, Long> {
    List<ConferenceRoom> findByNameIgnoreCase(String name);
}