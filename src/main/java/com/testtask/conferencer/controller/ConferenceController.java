package com.testtask.conferencer.controller;

import java.util.List;

import com.testtask.conferencer.entity.Conference;
import com.testtask.conferencer.entity.ConferenceRoom;
import com.testtask.conferencer.entity.Participant;
import com.testtask.conferencer.exception.ErrorInRequestException;
import com.testtask.conferencer.exception.ResourceNotFoundException;
import com.testtask.conferencer.repository.ConferenceRepository;
import com.testtask.conferencer.repository.ConferenceRoomRepository;
import com.testtask.conferencer.repository.ParticipantRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class ConferenceController {

    @Autowired
    private ConferenceRepository conferenceRepo;

    @Autowired
    private ParticipantRepository participantRepo;

    @Autowired
    private ConferenceRoomRepository conferenceRoomRepo;

    @CrossOrigin
    @RequestMapping(value = "/conference", method = RequestMethod.GET)
    public ResponseEntity<List<Conference>> getConferences() {
        return ResponseEntity.ok(conferenceRepo.findAll());
    }

    @CrossOrigin
    @RequestMapping(value = "/conference/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getConference(@PathVariable Long id) {
        return conferenceRepo.findById(id).map(conf -> {
            return ResponseEntity.ok(conf);
        }).orElseThrow(() -> new ResourceNotFoundException("Conference with id [" + id + "] not found"));
    }

    @CrossOrigin
    @RequestMapping(value = "/conference/create", method = RequestMethod.POST)
    public ResponseEntity<Conference> createConference(@RequestBody Conference confRequest) {
        return ResponseEntity.ok(conferenceRepo.save(confRequest));
    }

    @CrossOrigin
    @RequestMapping(value = "/conference/{id}/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateConference(@PathVariable Long id, @RequestBody Conference confRequest) {
        return conferenceRepo.findById(id).map(conf -> {

            return ResponseEntity.ok(conferenceRepo.save(
                conf.setName(confRequest.getName())
                    .setDateTime(confRequest.getDateTime())
                    .setConferenceRoom(confRequest.getConferenceRoom())));

        }).orElseThrow(() -> new ResourceNotFoundException("Conference with id [" + id + "] not found"));
    }

    @CrossOrigin
    @RequestMapping(value = "/conference/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeConference(@PathVariable Long id) {
        return conferenceRepo.findById(id).map(conf -> {
            conferenceRepo.delete(conf.setConferenceRoom(null));
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Conference with id [" + id + "] not found"));
    }

    @CrossOrigin
    @RequestMapping(value = "/room", method = RequestMethod.GET)
    public ResponseEntity<List<ConferenceRoom>> getRooms() {
        return ResponseEntity.ok(conferenceRoomRepo.findAll());
    }

    @CrossOrigin
    @RequestMapping(value = "/conference/{confId}/participant", method = RequestMethod.GET)
    public ResponseEntity<List<Participant>> getParticipantsByConference(@PathVariable Long confId) {
        return ResponseEntity.ok(participantRepo.findByConferenceId(confId));
    }

    @CrossOrigin
    @RequestMapping(value = "/conference/{confId}/participant", method = RequestMethod.POST)
    public ResponseEntity<?> addParticipantToConference(@PathVariable Long confId,
            @RequestBody Participant participantRequest) {

        return conferenceRepo.findById(confId).map(conf -> {
            
            if (participantRepo.countByConferenceId(confId) >= conf.getConferenceRoom().getMaxSeats()) {
                throw new ErrorInRequestException("Conference room is full!");
            }

            participantRequest.setConference(conf);
            return ResponseEntity.ok(participantRepo.save(participantRequest));
        }).orElseThrow(() -> new ResourceNotFoundException("Conference with id [" + confId + "] not found"));
    }

    @CrossOrigin
    @RequestMapping(value = "/conference/{confId}/participant/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeParticipantFromConference(@PathVariable Long confId, @PathVariable Long id) {

        if (!conferenceRepo.existsById(confId)) {
            throw new ResourceNotFoundException("Conference with id [" + confId + "] not found");
        }

        return participantRepo.findById(id).map(participant -> {
            participantRepo.delete(participant);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Participant with id [" + id + "] not found"));
    }
}