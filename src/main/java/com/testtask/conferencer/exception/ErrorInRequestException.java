package com.testtask.conferencer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ErrorInRequestException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ErrorInRequestException() {
        super();
    }

    public ErrorInRequestException(String message) {
        super(message);
    }

    public ErrorInRequestException(String message, Throwable cause) {
        super(message, cause);
    }
    
}