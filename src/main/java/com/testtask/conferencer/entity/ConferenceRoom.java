package com.testtask.conferencer.entity;

import javax.persistence.Entity;

/**
 * ConferenceRoom entity
 */
@Entity
public class ConferenceRoom extends Identifiable {

    private String name;

    private String location;

    private int maxSeats;

	public ConferenceRoom() { }

	public ConferenceRoom(String name, String location, int maxSeats) {
		this.name = name;
		this.location = location;
		this.maxSeats = maxSeats;
	}

	public String getName() { return name; }
	public String getLocation() { return location; }
	public int getMaxSeats() { return maxSeats; }

	public ConferenceRoom setName(String name) {
		this.name = name;
		return this;
    }

	public ConferenceRoom setLocation(String location) {
		this.location = location;
		return this;
	}

	public ConferenceRoom setMaxSeats(int maxSeats) {
		this.maxSeats = maxSeats;
		return this;
	}

}