package com.testtask.conferencer.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * The base class for all entities, stores Id for each entity
 */
@MappedSuperclass
public abstract class Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	public Long getId() { return id; }

	public Identifiable setId(Long id) {
		this.id = id;
		return this;
	}
    
}