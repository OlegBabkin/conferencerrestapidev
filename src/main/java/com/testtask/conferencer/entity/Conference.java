package com.testtask.conferencer.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * Conference entity
 */
@Entity
public class Conference extends Identifiable {

    private String name;

    private LocalDateTime dateTime;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "room_id")
	private ConferenceRoom conferenceRoom;

    public Conference() { }

	public Conference(String name, LocalDateTime dateTime) {
        this.name = name;
        this.dateTime = dateTime;
	}

	public String getName() { return name; }
	public LocalDateTime getDateTime() { return dateTime; }
	public ConferenceRoom getConferenceRoom() { return conferenceRoom; }

	public Conference setName(String name) {
		this.name = name;
		return this;
    }

	public Conference setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
		return this;
	}

	public Conference setConferenceRoom(ConferenceRoom conferenceRoom) {
		this.conferenceRoom = conferenceRoom;
		return this;
	}
	
}