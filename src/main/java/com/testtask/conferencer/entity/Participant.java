package com.testtask.conferencer.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * Participant entity
 */
@Entity
public class Participant extends Identifiable {

    private String name;

	private LocalDate birthDate;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "conference_id")
	@JsonIgnore
	private Conference conference;

	public Participant() { }

	public Participant(String name, LocalDate birthDate) {
		this.name = name;
		this.birthDate = birthDate;
	}

	public String getName() { return name; }
	public LocalDate getBirthDate() { return birthDate; }
	public Conference getConference() { return conference; }

	public Participant setName(String name) {
		this.name = name;
		return this;
	}
	
	public Participant setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
		return this;
	}

	public Participant setConference(Conference conference) {
		this.conference = conference;
		return this;
	}

}