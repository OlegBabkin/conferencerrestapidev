package com.testtask.conferencer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConferencerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConferencerApplication.class, args);
	}
}
