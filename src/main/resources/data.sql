INSERT INTO conference_room(name, location, max_seats) VALUES ('M/S Baltic Queen conference', 'M/S Baltic Queen', 124);
INSERT INTO conference_room(name, location, max_seats) VALUES ('Codeborne', 'Sepise 8, 11415 Tallinn', 4);

INSERT INTO conference(name, date_time, room_id) VALUES ('Fast startup for Java apps with GraalVM', CURRENT_TIMESTAMP, 1);
INSERT INTO conference(name, date_time, room_id) VALUES ('What the hack is blockchain', CURRENT_TIMESTAMP, 2);

INSERT INTO participant(name, birth_date, conference_id) VALUES ('John Doe', parsedatetime('01-01-1990','dd-mm-yyyy'), 1);
INSERT INTO participant(name, birth_date, conference_id) VALUES ('Mary Good', parsedatetime('01-01-1990','dd-mm-yyyy'), 1);
INSERT INTO participant(name, birth_date, conference_id) VALUES ('Sarah McLaren', parsedatetime('01-01-1990','dd-mm-yyyy'), 2);
INSERT INTO participant(name, birth_date, conference_id) VALUES ('Mary Good', parsedatetime('01-01-1990','dd-mm-yyyy'), 2);
INSERT INTO participant(name, birth_date, conference_id) VALUES ('Vova Petrov', parsedatetime('01-01-1990','dd-mm-yyyy'), 2);