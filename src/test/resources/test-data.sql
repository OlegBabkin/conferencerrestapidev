INSERT INTO conference_room(name, location, max_seats) VALUES ('name1', 'location1', 4);

INSERT INTO conference(name, date_time, room_id) VALUES ('confName1', CURRENT_TIMESTAMP, 1);
INSERT INTO conference(name, date_time, room_id) VALUES ('confName2', CURRENT_TIMESTAMP, 1);

INSERT INTO participant(name, birth_date, conference_id) VALUES ('John Doe', parsedatetime('01-01-1990','dd-mm-yyyy'), 1);
INSERT INTO participant(name, birth_date, conference_id) VALUES ('Mary Good', parsedatetime('01-01-1990','dd-mm-yyyy'), 1);
INSERT INTO participant(name, birth_date, conference_id) VALUES ('Sarah McLaren', parsedatetime('01-01-1990','dd-mm-yyyy'), 2);
INSERT INTO participant(name, birth_date, conference_id) VALUES ('Mary Good', parsedatetime('01-01-1990','dd-mm-yyyy'), 2);
INSERT INTO participant(name, birth_date, conference_id) VALUES ('Vova Petrov', parsedatetime('01-01-1990','dd-mm-yyyy'), 2);
