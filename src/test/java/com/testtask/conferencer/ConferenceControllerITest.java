package com.testtask.conferencer;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.mockMvc;
import static org.hamcrest.Matchers.equalTo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import com.testtask.conferencer.entity.Conference;
import com.testtask.conferencer.entity.Participant;
import com.testtask.conferencer.repository.ConferenceRepository;
import com.testtask.conferencer.repository.ParticipantRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = ConferencerApplication.class)
@AutoConfigureMockMvc
@Transactional
@TestPropertySource("classpath:test.properties")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ConferenceControllerITest {

    private static final String JSON = "application/json";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ConferenceRepository conferenceRepo;

    @Autowired
    private ParticipantRepository participantRepo;

    @Before
    public void setUp() {
        mockMvc(mockMvc);
    }

    @Test
    public void getConferences_whenRequestAll_thenOK() {
        given().
        when().
            get("/api/conference").
        then().
            statusCode(200).
            contentType(JSON).
            body("size()", equalTo(2)).
            body("[1].name", equalTo("confName2")).
            body("[1].id", equalTo(2));
    }
    
    @Test
    public void getConference_whenRequestOneConference_thenOK() {
        given().
        when().
            get("/api/conference/{id}", 2).
        then().
            statusCode(200).
            contentType(JSON).
            body("name", equalTo("confName2")).
            body("id", equalTo(2));
    }

    @Test
    public void getConference_whenRequestOneConference_thenNotFound() {
        given().
        when().
            get("/api/conference/{id}", 200).
        then().
            statusCode(404);
    }

    @Test
    public void createConference_whenPostConference_thenOkAndReturnsCreatedObject() {
        given().
            contentType(JSON).
            body(newConference(3)).
        when().
            post("/api/conference/create").
        then().
            statusCode(200).
            contentType(JSON).
            body("name", equalTo("confName3"));

        given().when().get("/api/conference").then().body("size()", equalTo(3));
    }
    
    @Test
    public void updateConference_whenPutConference_thenOkAndReturnsUpdatedObject() {
        Conference conference = conferenceRepo.
            findById(2L).
            map(conf -> conf.setName("updated name")).
            orElse(null);

        given().
            contentType(JSON).
            body(conference).
        when().
            put("/api/conference/{id}/update", 2).
        then().
            statusCode(200).
            contentType(JSON).
            body("name", equalTo("updated name"));
    }

    @Test
    public void updateConference_whenPutConferenceMissingId_thenNotFound() {
        given().
            contentType(JSON).
            body(newConference(10)).
        when().
            put("/api/conference/{id}/update", 10).
        then().
            statusCode(404);
    }

    @Test
    public void removeConference_whenDeleteConference_thenOk() {
        given().
        when().
            delete("/api/conference/{id}", 2).
        then().
            statusCode(200);

        given().when().get("/api/conference").then().body("size()", equalTo(1));
    }

    @Test
    public void removeConference_whenDeleteConferenceMissingId_thenNotFound() {
        given().
        when().
            delete("/api/conference/{id}", 10).
        then().
            statusCode(404);
        
        given().when().get("/api/conference").then().body("size()", equalTo(2));
    }

    @Test
    public void getRooms_whenGetAllRooms_thenOK() {
        given().
        when().
            get("/api/room").
        then().
            statusCode(200).
            contentType(JSON).
            body("size()", equalTo(1)).
            body("[0].name", equalTo("name1")).
            body("[0].id", equalTo(1));
    }

    @Test
    public void getParticipantsByConference_whenGetByConferenceId_thenOk() {
        given().
        when().
            get("/api/conference/{id}/participant", 2).
        then().
            statusCode(200).
            contentType(JSON).
            body("size()", equalTo(3)).
            body("[0].name", equalTo("Sarah McLaren")).
            body("[1].name", equalTo("Mary Good")).
            body("[2].name", equalTo("Vova Petrov"));
    }

    @Test
    public void getParticipantsByConference_whenGetByMissingConferenceId_thenEmptyArray() {
        given().
        when().
            get("/api/conference/{id}/participant", 20).
        then().
            statusCode(200).
            contentType(JSON).
            body("size()", equalTo(0));
    }

    @Test
    public void addParticipantToConference_whenPostParticipant_thenOk() {
        Conference conference = conferenceRepo.
            findById(2L).orElse(null);
        
        given().
            contentType(JSON).
            body(newParticipant("new name").setConference(conference)).
        when().
            post("/api/conference/{id}/participant", 2).
        then().
            statusCode(200).
            contentType(JSON).
            body("name", equalTo("new name"));

        given().when().get("/api/conference/2/participant").then().body("size()", equalTo(4));
    }

    @Test
    public void addParticipantToConference_whenRoomFull_thenBadRequest() {
        Conference conference = conferenceRepo.
            findById(2L).orElse(null);
        participantRepo.save(newParticipant("new name").setConference(conference));
        
        given().
            contentType(JSON).
            body(newParticipant("bad name").setConference(conference)).
        when().
            post("/api/conference/{id}/participant", 2).
        then().
            statusCode(400);

        given().when().get("/api/conference/2/participant").then().body("size()", equalTo(4));
    }

    @Test
    public void removeParticipantFromConference_whenRemove_thenOk() {
        given().
        when().
            delete("/api/conference/{confId}/participant/{id}", 2, 4).
        then().
            statusCode(200);

        given().when().get("/api/conference/2/participant").then().body("size()", equalTo(2));
    }

    /**
     * Generates new conference object with name + number, given as param
     */
    private Conference newConference(int num) {
        return new Conference().setName("confName" + num).
            setDateTime(LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0));
    }
    
    /**
     * Generates new Participant object with given name
     */
    private Participant newParticipant(String name) {
        return new Participant(name, LocalDate.of(1980, 1, 1));
    }
}