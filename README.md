# Conferencer

Rest API server for Tallink test task (Spring Boot, Maven, H2)

## Structure

* Entity – POJOs for orm, used uni-directional many-to-one relationship mappings.
* Repository – data access business logic, using JpaRepository interfaces. (Service layer was excluded for simplicity).
* Controller – Rest controllers
* Exception – Runtime exceptions, used by controller methods to send 400 and 404 errors to the client

## Notes

* Instead of adding CORS headers manually to each controller method, used CrossOrigin annotation.
* Integration testing performed with RestAssuredMockMvc and separately defined DB.
* Running dev server locally on windows: <\root-folder> .\mvnw spring-boot:run